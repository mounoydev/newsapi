var express = require('express');
require('dotenv').config()
var router = express.Router();
var jwt = require('jsonwebtoken');
const pool = require("../config_db");

/* GET users listing. */
router.get('/',async function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KAY, function(err, decoded) {
  });
  let conn;
  try {
	conn = await pool.getConnection();
  const rows = await conn.query("SELECT * FROM newsapp.users;");
  res.json(rows);
  } catch (err) {
  res.json(err);
  throw err;
  } finally {
	if (conn) conn.release(); //release to pool
  }
});
// Rounter login
router.post('/login', async function(req, res, next) {
  let conn;
  try {
	conn = await pool.getConnection(); // Create connection
  const rows = await conn.query("SELECT u.id,u.user,u.role FROM newsapp.users u  where `user` =? and pass=? and stt ='1'",
  [req.body.user,req.body.pass]);
  // Query data with parameter
    console.log(rows)
    if(rows[0]){    // Check Have Data in Rows
      var token = jwt.sign(rows[0], process.env.API_KEY);  // Create Token With Kay  | Row[0] is playload
      // 
   //   let conn;
      console.log(token,rows[0].id)
      const inserttoken = await conn.query("update users set tk=? WHERE id=?",
      [token,rows[0].id]);

      res.json({tk:token}) // Res Token to App
    }
    else
    {
      res.json({err:"somting worring"});
    }
  } catch (err) {
  res.json(err);
  throw err;
  } finally {
	if (conn) conn.release(); //release to pool (End connection)
  }
});
router.patch('/',async function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY, async function(err, decoded) {
    //console.log(decoded) // bar
    var userid =decoded.id;
    var userid =decoded.id;

    let conn;
    try {
    conn = await pool.getConnection();

    const rows = await conn.query("update newsapp.users set pass =? WHERE `users` .id =? and stt='1';",[req.body.pass,userid]);

    res.json(rows);

    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    res.json(decoded.id);
  });

  
});
router.post('/newuser',async function(req, res, next) {
  // Verity Token from Headers & apikey
    let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("INSERT into users(user,pass,stt) value(?,?,1);", [req.body.user,req.body.pass]);
    res.json(rows);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    res.json(decoded.id);

});

router.delete('/', function(req, res, next) {
  res.json('Users');
});

module.exports = router;
