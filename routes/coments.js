var express = require('express');
require('dotenv').config()
var router = express.Router();
var jwt = require('jsonwebtoken');

// call config_db.js
const pool = require("../config_db");

/* GET users listing. */
router.get('/:nid', function(req, res, next) {
    jwt.verify(req.headers.token, process.env.API_KEY, async function(err, decoded) {
        console.log(decoded) // bar
        if (decoded) {
            let conn;
            try {
                conn = await pool.getConnection();
                const rows = await conn.query("SELECT c.comment,c.`date`,u.`user` from comments c,users u where u.id = c.uid and  c.stt=1 and nid=?",[req.params.nid]); 
                // FIX 
                res.json(rows);
            } catch (err) {
                res.json(err);
                throw err;
            } finally {
                if (conn) conn.release(); //release to pool
            }
        } else {
            res.json({ "err": "No JWT" });
        }

    });
});

/* GET users listing by id */
router.get('/user/comment', function(req, res, next) {
    jwt.verify(req.headers.token, process.env.API_KEY, async function(err, decoded) {
        var uid = decoded.id;
        if (uid) {
            let conn;
            try {
                conn = await pool.getConnection();
                const rows = await conn.query("SELECT id, comment, date, nid, uid, stt from comments where stt=1 and uid =? ", [uid]);
                res.json(rows);
            } catch (err) {
                res.json(err);
                throw err;
            } finally {
                if (conn) conn.release(); //release to pool
            }
        } else {
            res.json({ "err": "No JWT" });
        }

    });
});

// insert comment info
router.post('/', function(req, res, next) {
    jwt.verify(req.headers.token, process.env.API_KEY, async function(err, decoded) {
        var uid = decoded.id;
        if (uid) {
            let conn;
            try {
                conn = await pool.getConnection();
                const rows = await conn.query("INSERT into comments (comment, date, nid, uid, stt) values(?,?,?,?,1)",
                [req.body.comment, new Date(), req.body.nid, uid]);
                res.json(rows);
            } catch (err) {
                res.json(err);
                throw err;
            } finally {
                if (conn) conn.release(); //release to pool
            }
        } else {
            res.json({ "err": "No JWT" });
        }

    });
});
// update comment info
router.put('/:id', function(req, res, next) {
    jwt.verify(req.headers.token, process.env.API_KEY, async function(err, decoded) {
        console.log(decoded) // bar
        var uid = decoded.id;
        if (decoded) {
            let conn;
            try {
                conn = await pool.getConnection();
                const rows = await conn.query("UPDATE comments set comment=?, date=now() where id =? and uid=?", [req.body.comment, req.params.id,uid]);
                res.json(rows);
            } catch (err) {
                res.json(err);
                throw err;
            } finally {
                if (conn) conn.release(); //release to pool
            }
        } else {
            res.json({ "err": "No JWT" });
        }

    });
});

//delete or update comment info
router.delete('/:id', function(req, res, next) {
    jwt.verify(req.headers.token, process.env.API_KEY, async function(err, decoded) {
        console.log(decoded) // bar
        if (decoded) {
            let conn;
            try {
                conn = await pool.getConnection();
                const rows = await conn.query("UPDATE comments set stt=0 where id =?", [req.params.id]);
                res.json(rows);
            } catch (err) {
                res.json(err);
                throw err;
            } finally {
                if (conn) conn.release(); //release to pool
            }
        } else {
            res.json({ "err": "No JWT" });
        }

    });
});

module.exports = router;